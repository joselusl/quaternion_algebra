
#include "quaternion_algebra/quaternion_algebra.h"



namespace Quaternion
{

Jacobians::Jacobians()
{
    std::vector<Eigen::Triplet<double> > triplets;

    // Jacobian error quaternion wrt error theta
    mat_diff_error_quat_wrt_error_theta_dense<<0, 0, 0,
                                            0.5, 0, 0,
                                            0, 0.5, 0,
                                            0, 0, 0.5;

    mat_diff_error_quat_wrt_error_theta_sparse.resize(4,3);
    triplets.clear();
    triplets.push_back(Eigen::Triplet<double>(1, 0, 0.5));
    triplets.push_back(Eigen::Triplet<double>(2, 1, 0.5));
    triplets.push_back(Eigen::Triplet<double>(3, 2, 0.5));
    mat_diff_error_quat_wrt_error_theta_sparse.setFromTriplets(triplets.begin(), triplets.end());



    // Jacobian error theta wrt error quaternion
    mat_diff_error_theta_wrt_error_quat_dense<<0, 2, 0, 0,
                                                0, 0, 2, 0,
                                                0, 0, 0, 2;

    mat_diff_error_theta_wrt_error_quat_sparse.resize(3, 4);
    triplets.clear();
    triplets.push_back(Eigen::Triplet<double>(0, 1, 2));
    triplets.push_back(Eigen::Triplet<double>(1, 2, 2));
    triplets.push_back(Eigen::Triplet<double>(2, 3, 2));
    mat_diff_error_theta_wrt_error_quat_sparse.setFromTriplets(triplets.begin(), triplets.end());



    mat_diff_quat_inv_wrt_quat_dense<<1, 0, 0, 0,
                                        0, -1, 0, 0,
                                        0, 0, -1, 0,
                                        0, 0, 0, -1;

    mat_diff_quat_inv_wrt_quat_sparse.resize(4, 4);
    triplets.clear();
    triplets.push_back(Eigen::Triplet<double>(0, 0, 1));
    triplets.push_back(Eigen::Triplet<double>(1, 1, -1));
    triplets.push_back(Eigen::Triplet<double>(2, 2, -1));
    triplets.push_back(Eigen::Triplet<double>(3, 3, -1));
    mat_diff_quat_inv_wrt_quat_sparse.setFromTriplets(triplets.begin(), triplets.end());



    mat_diff_vector_wrt_vector_amp_dense<<0, 1, 0, 0,
                                            0, 0, 1, 0,
                                            0, 0, 0, 1;

    mat_diff_vector_wrt_vector_amp_sparse.resize(3, 4);
    triplets.clear();
    triplets.push_back(Eigen::Triplet<double>(0, 1, 1));
    triplets.push_back(Eigen::Triplet<double>(1, 2, 1));
    triplets.push_back(Eigen::Triplet<double>(2, 3, 1));
    mat_diff_vector_wrt_vector_amp_sparse.setFromTriplets(triplets.begin(), triplets.end());


    mat_diff_vector_amp_wrt_vector_dense<<0, 0, 0,
                                            1, 0, 0,
                                            0, 1, 0,
                                            0, 0, 1;

    mat_diff_vector_amp_wrt_vector_sparse.resize(4, 3);
    triplets.clear();
    triplets.push_back(Eigen::Triplet<double>(1, 0, 1));
    triplets.push_back(Eigen::Triplet<double>(2, 1, 1));
    triplets.push_back(Eigen::Triplet<double>(3, 2, 1));
    mat_diff_vector_amp_wrt_vector_sparse.setFromTriplets(triplets.begin(), triplets.end());


    // End
    return;
}


Jacobians jacobians;



Quaternion conj(const Quaternion &q)
{
    Quaternion qr;

    qr[0]=q[0];
    qr.block<3,1>(1,0)=-q.block<3,1>(1,0);

    return qr;
}


Quaternion inv(const Quaternion& q)
{
    Quaternion qr;


    qr=conj(q)/q.norm();


    return qr;
}



Quaternion cross(const Quaternion& q1, const Quaternion& q2)
{
    Quaternion qres;


    // Quat 1
    double pr=q1[0];
    Eigen::Vector3d pv=q1.block<3,1>(1,0);

    // Quat 2
    double qr=q2[0];
    Eigen::Vector3d qv=q2.block<3,1>(1,0);

    // Result
    qres[0]=pr*qr-pv.transpose()*qv;
    qres.block<3,1>(1,0)=pr*qv+qr*pv+pv.cross(qv);


    return qres;
}

Quaternion cross_gen_pure(const Quaternion &q1, const Eigen::Vector3d &q2)
{
    Quaternion qres;

    // Quat 1
    double pr=q1[0];
    Eigen::Vector3d pv=q1.block<3,1>(1,0);

    // Quat 2
    // qr=0
    // qv=q2


    // Result
    qres[0]=-pv.transpose()*q2;
    qres.block<3,1>(1,0)=pr*q2+pv.cross(q2);


    return qres;
}

Quaternion cross_pure_gen(const Eigen::Vector3d& q1, const Quaternion& q2)
{
    Quaternion qres;


    // Quat 1
    // pr=0
    // pv=q1

    // Quat 2
    double qr=q2[0];
    Eigen::Vector3d qv=q2.block<3,1>(1,0);

    // Result
    qres[0]=-q1.transpose()*qv;
    qres.block<3,1>(1,0)=qr*q1+q1.cross(qv);


    return qres;
}

Quaternion cross_pure_pure(const Eigen::Vector3d& q1, const Eigen::Vector3d& q2)
{
    Quaternion qres;


    // Quat 1
    //pr=0;
    //pv=q1;

    // Quat 2
    //qr=0;
    //qv=q2;

    // Result
    qres[0]=-q1.transpose()*q2;
    qres.block<3,1>(1,0)=q1.cross(q2);


    return qres;

}


Eigen::Vector3d cross_sandwich(const Quaternion &q1, const Eigen::Vector3d &q2, const Quaternion &q3)
{
    Quaternion qres;

    qres = cross(q1, cross_pure_gen(q2, q3));

    return qres.block<3,1>(1,0);
}


// Q+
Eigen::Matrix4d quatMatPlus(const Quaternion &q)
{
    Eigen::Matrix4d QuatMat=q[0]*Eigen::Matrix4d::Identity(4,4);

    QuatMat.block<1,3>(0,1)+=-(q.block<3,1>(1,0)).transpose();
    QuatMat.block<3,3>(1,1)+=skewSymMat(q.block<3,1>(1,0));
    QuatMat.block<3,1>(1,0)+=(q.block<3,1>(1,0));

    return QuatMat;
}

Eigen::Matrix4d quatMatPlus(const Eigen::Vector3d& q)
{
//    Quaternion quat;
//    quat[0]=0;
//    quat.block<3,1>(1,0)=q;

//    return quatMatPlus(quat);

    Eigen::Matrix4d QuatMat;
    QuatMat.setZero();

    QuatMat.block<1,3>(0,1)=-q.transpose();
    QuatMat.block<3,3>(1,1)=skewSymMat(q);
    QuatMat.block<3,1>(1,0)=q;

    return QuatMat;
}


// Q-
Eigen::Matrix4d quatMatMinus(const Quaternion& q)
{
    Eigen::Matrix4d QuatMat=q[0]*Eigen::Matrix4d::Identity(4,4);

    QuatMat.block<1,3>(0,1)+=-(q.block<3,1>(1,0)).transpose();
    QuatMat.block<3,3>(1,1)+=-skewSymMat(q.block<3,1>(1,0));
    QuatMat.block<3,1>(1,0)+=(q.block<3,1>(1,0));

    return QuatMat;
}

Eigen::Matrix4d quatMatMinus(const Eigen::Vector3d& q)
{
//    Quaternion quat;
//    quat[0]=0;
//    quat.block<3,1>(1,0)=q;

//    return quatMatMinus(quat);

    Eigen::Matrix4d QuatMat;
    QuatMat.setZero();

    QuatMat.block<1,3>(0,1)=-q.transpose();
    QuatMat.block<3,3>(1,1)=-skewSymMat(q);
    QuatMat.block<3,1>(1,0)=q;

    return QuatMat;
}


Quaternion rotationVectorToQuaternion(const Eigen::Vector3d& v_rot)
{
    Quaternion quat;

    double norm_v=v_rot.norm();

    if(norm_v<1e-3)
    {
        quat[0]=1;
        quat.block<3,1>(1,0)=0.5*v_rot;
    }
    else
    {
        quat[0]=cos(norm_v/2);
        quat.block<3,1>(1,0)=v_rot/norm_v*sin(norm_v/2);
    }

    return quat;
}


Eigen::Matrix<double, 4, 3> jacobianRotationVectorToQuaternion(const Eigen::Vector3d& v_rot)
{
    Eigen::Matrix<double, 4, 3> jacobian_matrix;//(4,3);
    jacobian_matrix.setZero();


    double norm_v=v_rot.norm();

    if(norm_v<1e-3)
    {
        jacobian_matrix(1,0)=0.5;
        jacobian_matrix(2,1)=0.5;
        jacobian_matrix(3,2)=0.5;
    }
    else
    {
        jacobian_matrix.block<1,3>(0,0)=-v_rot.transpose()/(2*norm_v)*sin(norm_v/2);
        jacobian_matrix.block<3,3>(1,0)=1/(pow(norm_v,3))*(Eigen::Matrix3d::Identity()*pow(norm_v,2)-v_rot*v_rot.transpose())*sin(norm_v/2)+0.5*v_rot*v_rot.transpose()/(pow(norm_v,2))*cos(norm_v/2);
    }


    return jacobian_matrix;
}


Eigen::Matrix3d skewSymMat(const Eigen::Vector3d& w)
{
    Eigen::Matrix3d skewSymMat;
    skewSymMat<<0, -w[2], w[1],
                w[2], 0, -w[0],
                -w[1], w[0], 0;

    return skewSymMat;
}

Eigen::Matrix3d quaternionToRotationMatrix(const Quaternion& q)
{
    Eigen::Matrix3d rotMat;

    rotMat<< 1-2*std::pow(q[2],2)-2*std::pow(q[3],2),   2*q[1]*q[2]-2*q[0]*q[3],                    2*q[1]*q[3]+2*q[0]*q[2],
             2*q[1]*q[2]+2*q[0]*q[3],                   1-2*std::pow(q[1],2)-2*std::pow(q[3],2),    2*q[2]*q[3]-2*q[0]*q[1],
             2*q[1]*q[3]-2*q[0]*q[2],                   2*q[2]*q[3]+2*q[0]*q[1],                    1-2*std::pow(q[1],2)-2*std::pow(q[2],2);

    return rotMat;
}

Quaternion eulerAnglesToQuaternion(const Eigen::Vector3d& euler_angles, EulerAngleRotationOrder rotation_order)
{
    return eulerAnglesToQuaternion(euler_angles[0], euler_angles[1], euler_angles[2], rotation_order);
}

Quaternion eulerAnglesToQuaternion(double euler_angles_yaw, double euler_angles_pitch, double euler_angles_roll, EulerAngleRotationOrder rotation_order)
{
    Quaternion quaternion;

    // Precalculations
    double c1=std::cos(euler_angles_yaw/2);
    double s1=std::sin(euler_angles_yaw/2);
    double c2=std::cos(euler_angles_pitch/2);
    double s2=std::sin(euler_angles_pitch/2);
    double c3=std::cos(euler_angles_roll/2);
    double s3=std::sin(euler_angles_roll/2);

    // Computation: from matlab
    switch(rotation_order)
    {
        case EulerAngleRotationOrder::zyx:
        {
            quaternion[0] = c1 * c2 * c3 + s1 * s2 * s3;
            quaternion[1] = c1 * c2 * s3 - s1 * s2 * c3;
            quaternion[2] = c1 * s2 * c3 + s1 * c2 * s3;
            quaternion[3] = s1 * c2 * c3 - c1 * s2 * s3;
            break;
        }
        default:
            throw 1;
    }

    return quaternion;
}

Eigen::Vector3d quaternionToEulerAngles(const Quaternion& quaternion, EulerAngleRotationOrder rotation_order)
{
    Eigen::Vector3d euler_angles;

    // Pre computation


    // Computation: from matlab
    switch(rotation_order)
    {
        case EulerAngleRotationOrder::zyx:
        {
            euler_angles[0] = std::atan2( 2*(quaternion[1]*quaternion[2] + quaternion[0]*quaternion[3]), std::pow(quaternion[0], 2) + std::pow(quaternion[1], 2) - std::pow(quaternion[2], 2) - std::pow(quaternion[3], 2) );
            euler_angles[1] = std::asin( -2*(quaternion[1]*quaternion[3] - quaternion[0]*quaternion[2]) );
            euler_angles[2] = std::atan2( 2*(quaternion[2]*quaternion[3] + quaternion[0]*quaternion[1]), std::pow(quaternion[0], 2) - std::pow(quaternion[1], 2) - std::pow(quaternion[2], 2) + std::pow(quaternion[3], 2) );
            break;
        }
        default:
            throw 1;
    }


    return euler_angles;
}

Quaternion quaternionInterpolation(const Quaternion& quaternion1, const Quaternion& quaternion2, double h, QuaternionInterpolationMethod interpolation_method)
{
    // Checks
    // Interpolation range
    if(h<0.0 || h>1.0)
    {
        std::cerr<<"Quaternion::quaternionInterpolation(). h is out of boundaries"<<std::endl;
        throw 1;
    }
    // Unit quaternions
    double tol=1e-3;
    if( std::abs(quaternion1.norm()-1.0) > tol )
    {
        std::cerr<<"Quaternion::quaternionInterpolation(). quaternion1 not unit quaternion: ["<<quaternion1[0]<<"; "<<quaternion1[1]<<"; "<<quaternion1[2]<<"; "<<quaternion1[3]<<"]"<<std::endl;
        throw 2;
    }
    if( std::abs(quaternion2.norm()-1.0) > tol )
    {
        std::cerr<<"Quaternion::quaternionInterpolation(). quaternion2 not unit quaternion: ["<<quaternion2[0]<<"; "<<quaternion2[1]<<"; "<<quaternion2[2]<<"; "<<quaternion2[3]<<"]"<<std::endl;
        throw 2;
    }

    if(h==0.0)
    {
        return quaternion1;
    }

    if(h==1.0)
    {
        return quaternion2;
    }

    // Return variable
    Quaternion interpolated_quaternion;

    if(interpolation_method == QuaternionInterpolationMethod::slerp)
    {
        Eigen::Quaterniond q1(quaternion1[0], quaternion1[1], quaternion1[2], quaternion1[3]);
        Eigen::Quaterniond q2(quaternion2[0], quaternion2[1], quaternion2[2], quaternion2[3]);



        Eigen::Quaterniond qres;


        qres = q1.slerp(h, q2);


        interpolated_quaternion[0]=qres.w();
        interpolated_quaternion[1]=qres.x();
        interpolated_quaternion[2]=qres.y();
        interpolated_quaternion[3]=qres.z();
    }


    // End
    return interpolated_quaternion;
}


}
