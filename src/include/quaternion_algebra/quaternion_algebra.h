#ifndef _QUATERNION_ALGEBRA_H
#define _QUATERNION_ALGEBRA_H


#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/Geometry> 


#include <cmath>

#include <iostream>


namespace Quaternion
{
    // Typedef
    typedef Eigen::Vector4d Quaternion;
    typedef Eigen::Vector3d PureQuaternion;

    /////////////////////////////////////////////
    //////////////////// Jacobians //////////////
    // Class for Usefull Jacobians related to Quaternions
    extern class Jacobians
    {
        public:
            Jacobians();

        public:
            // Jacobian error quaternion wrt error theta
            Eigen::Matrix<double, 4, 3> mat_diff_error_quat_wrt_error_theta_dense;
            Eigen::SparseMatrix<double> mat_diff_error_quat_wrt_error_theta_sparse;

            // Jacobian error theta wrt error quaternion
            Eigen::Matrix<double, 3, 4> mat_diff_error_theta_wrt_error_quat_dense;
            Eigen::SparseMatrix<double> mat_diff_error_theta_wrt_error_quat_sparse;

            //
            Eigen::Matrix<double, 4, 4> mat_diff_quat_inv_wrt_quat_dense;
            Eigen::SparseMatrix<double> mat_diff_quat_inv_wrt_quat_sparse;

            //
            Eigen::Matrix<double, 3, 4> mat_diff_vector_wrt_vector_amp_dense;
            Eigen::SparseMatrix<double> mat_diff_vector_wrt_vector_amp_sparse;

            //
            Eigen::Matrix<double, 4, 3> mat_diff_vector_amp_wrt_vector_dense;
            Eigen::SparseMatrix<double> mat_diff_vector_amp_wrt_vector_sparse;


    } jacobians;


    /////////////////////////////////////////////
    //////////////////// Operations //////////////

    // Conjugate
    Quaternion conj(const Quaternion& q);

    // Inverse
    Quaternion inv(const Quaternion &q);

    // Cross with two arguments
    Quaternion cross(const Quaternion& q1, const Quaternion& q2);
    Quaternion cross_gen_pure(const Quaternion& q1, const Eigen::Vector3d& q2);
    Quaternion cross_pure_gen(const Eigen::Vector3d& q1, const Quaternion& q2);
    Quaternion cross_pure_pure(const Eigen::Vector3d& q1, const Eigen::Vector3d& q2);

    // Cross with n arguments for general quaternions
    template <typename ...Tail>
    Quaternion cross(const Quaternion& head, Tail... tail)
    {
        return cross(head, cross(tail...));
    }

    // Sandwich product
     Eigen::Vector3d cross_sandwich(const Quaternion& q1, const Eigen::Vector3d& q2, const Quaternion& q3);

    // Quaternion Matrixes
    // Q+
    Eigen::Matrix4d quatMatPlus(const Quaternion& q);
    Eigen::Matrix4d quatMatPlus(const Eigen::Vector3d& q);
    // Q-
    Eigen::Matrix4d quatMatMinus(const Quaternion& q);
    Eigen::Matrix4d quatMatMinus(const Eigen::Vector3d& q);




    /////////////////////////////////////////////
    //////////////////// Interpolation //////////////

    // Quaternion interpolation
    // TODO
    enum class QuaternionInterpolationMethod
    {
        slerp,
        lerp,
        nlerp
    };
    Quaternion quaternionInterpolation(const Quaternion& quaternion1, const Quaternion& quaternion2, double h, QuaternionInterpolationMethod interpolation_method=QuaternionInterpolationMethod::slerp);





    /////////////////////////////////////////////
    //////////////////// Conversions //////////////

    // Rotation vector to quaternion
    Quaternion rotationVectorToQuaternion(const Eigen::Vector3d& v_rot);

    Eigen::Matrix<double, 4, 3> jacobianRotationVectorToQuaternion(const Eigen::Vector3d& v_rot);


    // Skew-Symmetric Matrix: https://en.wikipedia.org/wiki/Skew-symmetric_matrix
    Eigen::Matrix3d skewSymMat(const Eigen::Vector3d& w);

    // Quaternion to rotation matrix
    Eigen::Matrix3d quaternionToRotationMatrix(const Quaternion& q);

    // Euler angles to quaternion
    // TODO
    enum class EulerAngleRotationOrder
    {
        zyx,
        zyz,
        zxy,
        zxz,
        yxz,
        yxy,
        yzx,
        yzy,
        xyz,
        xyx,
        xzy,
        xzx
    };

    Quaternion eulerAnglesToQuaternion(const Eigen::Vector3d& euler_angles, EulerAngleRotationOrder rotation_order=EulerAngleRotationOrder::zyx);
    Quaternion eulerAnglesToQuaternion(double euler_angles_yaw, double euler_angles_pitch, double euler_angles_roll, EulerAngleRotationOrder rotation_order=EulerAngleRotationOrder::zyx);

    Eigen::Vector3d quaternionToEulerAngles(const Quaternion& quaternion, EulerAngleRotationOrder rotation_order=EulerAngleRotationOrder::zyx);




}






#endif
