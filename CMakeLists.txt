cmake_minimum_required(VERSION 2.8.3)

project(quaternion_algebra)


# check c++11 / c++0x
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
    #set(CMAKE_CXX_FLAGS "-std=c++11")
    # http://stackoverflow.com/questions/37621342/cmake-will-not-compile-to-c-11-standard
    # CMake below 3.1
    add_definitions(-std=c++11)
    # CMake 3.1 and above
    set(CMAKE_CXX_STANDARD 11) # C++11...
    set(CMAKE_CXX_STANDARD_REQUIRED ON) #...is required...
    set(CMAKE_CXX_EXTENSIONS OFF) #...without compiler extensions like gnu++11
elseif(COMPILER_SUPPORTS_CXX0X)
    #set(CMAKE_CXX_FLAGS "-std=c++0x")
    add_definitions(-std=c++0x)
    message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
else()
    message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()


# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries

 
#set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall")

#set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall")
#set(DCMAKE_BUILD_TYPE Release)


#QUATERNION_ALGEBRA
set(QUATERNION_ALGEBRA_SOURCE_DIR
	src/source) 
	
set(QUATERNION_ALGEBRA_INCLUDE_DIR
	src/include)

set(QUATERNION_ALGEBRA_SOURCE_FILES
  ${QUATERNION_ALGEBRA_SOURCE_DIR}/quaternion_algebra.cpp
)
 
set(QUATERNION_ALGEBRA_HEADER_FILES
  ${QUATERNION_ALGEBRA_INCLUDE_DIR}/quaternion_algebra/quaternion_algebra.h
)




find_package(catkin REQUIRED
                COMPONENTS)


find_package(Eigen3)
if(NOT EIGEN3_FOUND)
  # Fallback to cmake_modules
  find_package(cmake_modules REQUIRED)
  find_package(Eigen REQUIRED)
  set(EIGEN3_INCLUDE_DIRS ${EIGEN_INCLUDE_DIRS})
  set(EIGEN3_LIBRARIES ${EIGEN_LIBRARIES})  # Not strictly necessary as Eigen is head only
  # Possibly map additional variables to the EIGEN3_ prefix.
  message(WARN "Using Eigen2!")
else()
  set(EIGEN3_INCLUDE_DIRS ${EIGEN3_INCLUDE_DIR})
endif()



catkin_package(
        INCLUDE_DIRS ${QUATERNION_ALGEBRA_INCLUDE_DIR}
        LIBRARIES ${PROJECT_NAME}
        DEPENDS EIGEN3
        CATKIN_DEPENDS
  )


include_directories(${QUATERNION_ALGEBRA_INCLUDE_DIR})
include_directories(${EIGEN3_INCLUDE_DIRS})
include_directories(${catkin_INCLUDE_DIRS})



add_library(${PROJECT_NAME} ${QUATERNION_ALGEBRA_SOURCE_FILES} ${QUATERNION_ALGEBRA_HEADER_FILES})
add_dependencies(${PROJECT_NAME} ${catkin_EXPORTED_TARGETS})
target_link_libraries(${PROJECT_NAME} ${EIGEN3_LIBRARIES})
target_link_libraries(${PROJECT_NAME} ${catkin_LIBRARIES})

